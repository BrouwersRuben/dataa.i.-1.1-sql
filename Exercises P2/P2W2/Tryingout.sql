CREATE TABLE my_student
(ID NUMBER(2) CONSTRAINT my_stud_id_pk PRIMARY KEY,
first_name VARCHAR2(30),
last_name VARCHAR2(30));

CREATE TABLE my_subject
(ID NUMBER(2) CONSTRAINT my_sub_id_pk PRIMARY KEY,
name VARCHAR2(30));

CREATE TABLE my_enrollments
(grade NUMBER(100),
student_id NUMBER(2) REFERENCES my_student(id),
subject_id NUMBER(2) REFERENCES my_subject(id),
CONSTRAINT my_enrollments PRIMARY KEY (student_id, subject_id));

CREATE TABLE seq_d_songs
AS SELECT * FROM D_SONGS;

CREATE SEQUENCE seq_d_song_seq
    START WITH 100
    INCREMENT BY 2
    NOCACHE
    NOCYCLE;

INSERT INTO seq_d_songs VALUES
    (seq_d_song_seq.nextval, 'Island Fever', 5, 'Hawaii Islanders', 12);

SELECT * FROM seq_d_songs;

INSERT INTO seq_d_songs VALUES
    (seq_d_song_seq.nextval, 'Castle of Dreams', 4, 'The Wanderers', 77);

UPDATE seq_d_songs
SET DURATION = '4 min'
WHERE id = 102;

CREATE INDEX D_TRACK_LISTINGS_IX
    ON D_TRACK_LISTINGS (CD_NUMBER);

SELECT * FROM USER_INDEXES WHERE INDEX_NAME LIKE '%D_TRACK%'

CREATE SYNONYM my_tracks FOR D_TRACK_LISTINGS;

DROP INDEX D_TRACK_LISTINGS_IX;
DROP SYNONYM my_tracks;


