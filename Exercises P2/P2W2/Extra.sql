DROP TABLE my_orders PURGE;
DROP TABLE my_costumers PURGE;
DROP TABLE my_orderline PURGE;
DROP TABLE my_products PURGE;
DROP INDEX my_prod_desc_ix;


--EX1
CREATE TABLE my_costumers
(
    CostumerID NUMBER(3)
        CONSTRAINT my_cost_id_pk PRIMARY KEY,
    First_name VARCHAR2(15),
    Last_name VARCHAR2(15),
    Country VARCHAR2(15),
    City VARCHAR2(15),
    Postal_code NUMBER(4),
    Street VARCHAR2(15),
    House_number NUMBER(4)
);

CREATE TABLE my_orders
(
    OrderID NUMBER(3)
        CONSTRAINT my_ord_oid_pk PRIMARY KEY,
    CostumerID NUMBER(3)
        CONSTRAINT my_ord_ci_fk REFERENCES my_costumers(CostumerID),
    Order_Date DATE,
    Order_Location VARCHAR2(15)
);

CREATE TABLE my_orderline
(
        OrderlineID NUMBER(3)
            CONSTRAINT my_orderline_olid_pk PRIMARY KEY,
        OrderID NUMBER(3)
            CONSTRAINT my_orderline_oid_fk REFERENCES my_orders(OrderID),
        ProductID NUMBER(3)
            CONSTRAINT my_orderline_pid_fk REFERENCES my_products(ProductID),
        Quantity NUMBER(3)
);

CREATE TABLE my_products
(
    ProductID NUMBER(3)
        CONSTRAINT my_prod_pid_pk PRIMARY KEY,
    Description VARCHAR2(15),
    Available VARCHAR2(4),
    PurchasingPrice NUMBER(3),
    SellingPrice NUMBER(3),
    Weight NUMBER(3),
    Storage_Location VARCHAR2(15)
);

CREATE INDEX my_prod_desc_ix
ON my_products(Description);

